/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.albumproject.albumproject.poc;

import com.wip.albumproject.albumproject.model.ReportSale;
import com.wip.albumproject.albumproject.service.ReportService;
import java.util.List;

/**
 *
 * @author WIP
 */
public class TestReportSale {

    public static void main(String[] args) {
        ReportService reportService = new ReportService();
//        List<ReportSale> reportDay = reportService.getReportSaleByDay();
//        for (ReportSale r : reportDay) {
//            System.out.println(r);
//        }

        List<ReportSale> reportMonth = reportService.getReportSaleByMonth(2013);
        for (ReportSale r : reportMonth) {
            System.out.println(r);
        }
    }

}
